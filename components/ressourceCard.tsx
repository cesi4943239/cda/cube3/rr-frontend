import { Card } from "@codegouvfr/react-dsfr/Card";
import { Ressource } from "../types/ressource";

export default function RessourceCard({ ressource }: { ressource: Ressource }) {
    return (
        <div
        className="container"
        style={{
            width: 1000,
            marginBottom: 100
        }}
        >
        <Card
            background
            border
            desc="Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et"
            endDetail={`${ressource.author.firstname} ${ressource.author.lastname}`}
            enlargeLink
            horizontal
            imageAlt="texte alternatif de l’image"
            imageUrl="https://www.systeme-de-design.gouv.fr/img/placeholder.16x9.png"
            linkProps={{
              href: '#'
            }}
            size="large"
            title={ressource.title}
            titleAs="h3"
        >
        </Card>
    
        </div>
    )
}
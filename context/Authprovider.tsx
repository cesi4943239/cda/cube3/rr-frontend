import AsyncStorage from "@react-native-async-storage/async-storage";
import { Buffer } from "buffer";
import React, { ReactElement, createContext, useState } from "react";
import apiCall from "../lib/apiCall";

export const AuthContext = createContext<any>(undefined);

export default function AuthProvider({ children }: { children?: ReactElement }) {
    const [ userInfos, setUserInfo ] = useState<User>();

    // https://www.youtube.com/watch?v=QMUii9fSKfQ&t=1516s
    const getUserInfos: any = () => {
        return apiCall({
            url: "/user/",
            method: "GET",
        }).then(async (result: any) => {
            const res: Array<User> = result.data;
            const userId = await decodeToken().then((user) => user.userId);
            const user = res.find(users => users.id == userId)
            return setUserInfo(user);
        })
    }

    const login = (token: string) => {
        AsyncStorage.setItem('userToken', token)
        getUserInfos()
    }

    const logout = () => {
        AsyncStorage.removeItem('userToken');
        setUserInfo(undefined);
    }

    const decodeToken = async () => {
        const userToken = await AsyncStorage.getItem('userToken');
        if (userToken != null) return JSON.parse(Buffer.from(userToken.split('.')[1], 'base64').toString()); 
        return null;
    }

    return (
        <AuthContext.Provider value={{login, logout, userInfos}}>
            {children}
        </AuthContext.Provider>
    )
}

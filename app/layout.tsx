"use client";

import { NextAppDirEmotionCacheProvider } from "tss-react/next";
import { DsfrHead } from "@codegouvfr/react-dsfr/next-appdir/DsfrHead";
import { DsfrProvider } from "@codegouvfr/react-dsfr/next-appdir/DsfrProvider";
import { getHtmlAttributes } from "@codegouvfr/react-dsfr/next-appdir/getHtmlAttributes";
import { StartDsfr } from "./StartDsfr";
import { defaultColorScheme } from "./defaultColorScheme";
import MuiDsfrThemeProvider from "@codegouvfr/react-dsfr/mui";
import { Navigation } from "./Navigation";
import { Header } from "@codegouvfr/react-dsfr/Header";
import { Footer } from "@codegouvfr/react-dsfr/Footer";
import { Badge } from "@codegouvfr/react-dsfr/Badge";
import { headerFooterDisplayItem } from "@codegouvfr/react-dsfr/Display";
import { fr } from "@codegouvfr/react-dsfr";
import Link from "next/link";
import { useIsHeaderMenuModalOpen } from "@codegouvfr/react-dsfr/Header/useIsHeaderMenuModalOpen";
import { LoginHeaderItem } from "./LoginHeaderItem";
import {
	ConsentBannerAndConsentManagement,
	FooterConsentManagementItem,
	FooterPersonalDataPolicyItem
} from "../ui/consentManagement";

export default function RootLayout({ children }: { children: JSX.Element; }) {

	const isOpen = useIsHeaderMenuModalOpen();

	return (
		<html {...getHtmlAttributes({ defaultColorScheme })}>
			<head>
				<title>(RE)Sources Relationnelles</title>
				<StartDsfr />
				<DsfrHead
					Link={Link}
					preloadFonts={[
						//"Marianne-Light",
						//"Marianne-Light_Italic",
						"Marianne-Regular",
						//"Marianne-Regular_Italic",
						"Marianne-Medium",
						//"Marianne-Medium_Italic",
						"Marianne-Bold"
						//"Marianne-Bold_Italic",
						//"Spectral-Regular",
						//"Spectral-ExtraBold"
					]}
				/>
			</head>
			<body
				style={{
					"minHeight": "100vh",
					"display": "flex",
					"flexDirection": "column"
				}}
			>
				<DsfrProvider>
					<ConsentBannerAndConsentManagement />
					<NextAppDirEmotionCacheProvider options={{ "key": "css" }}>
						<MuiDsfrThemeProvider>
							<Header
								brandTop={<>RÉPUBLIQUE<br />FRANÇAISE</>}
								homeLinkProps={{
									href: '/',
									title: "Accueil - (RE)Sources Relationnelles"
								}}
								id="fr-header-with-uncontrolled-search-bar"
								onSearchButtonClick={function noRefCheck(){}}
								serviceTagline="Ressources et outils citoyens"
								serviceTitle={<>(RE)Sources Relationnelles{' '}<Badge as="span" noIcon severity="success">Beta</Badge></>}
								quickAccessItems={[
									headerFooterDisplayItem,
									{
										"iconId": "ri-mail-line",
										"linkProps": {
											"href": "mailto:contact@example.gouv.fr",
										},
										"text": "Contact us"
									},
									<LoginHeaderItem key={0} />
								]}
								navigation={<Navigation />}
							/>
							<div
								style={{
									"flex": 1,
									"margin": "auto",
									"maxWidth": 1000,
									...fr.spacing("padding", {
										"topBottom": "15v"
									})
								}}>
								{children}
							</div>
								<Footer
								accessibility="fully compliant"
								contentDescription="
									Comme exemple de contenu, vous pouvez indiquer les informations 
									suivantes : Le site officiel d’information administrative pour les entreprises.
									Retrouvez toutes les informations et démarches administratives nécessaires à la création, 
									à la gestion et au développement de votre entreprise.
									"
								/>
						</MuiDsfrThemeProvider>
					</NextAppDirEmotionCacheProvider>
				</DsfrProvider>
			</body>
		</html>
	);
}

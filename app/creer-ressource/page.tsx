"use client";

import * as React from "react";
import { Input } from "@codegouvfr/react-dsfr/Input";
import { Upload } from "@codegouvfr/react-dsfr/Upload";
import { Button } from "@codegouvfr/react-dsfr/Button";

export default function Page() {

    return(
        <div>
            <h1>Créez votre ressource</h1>
            <InputTitle />
            <InputDescription />
            <ButtonUploadImage />
            <ButtonValidate />
        </div>
    )
        
}

function InputTitle () {
    return (
    <Input
        label="Veuillez saisir le titre de votre ressource"
        state="default"
        stateRelatedMessage="Text de validation / d'explication de l'erreur"
      />
      )
}

function InputDescription () {
    return (
      <Input
      label="Veuillez saisir la description de votre ressource"
      textArea
    />
      )
}

function ButtonUploadImage () {
  return (
    <Upload
    label="Insérer une image"
    state="default"
    hint="Ajouter un media est optionnel. Vous pouvez ajouter une image ou une vidéo."
    stateRelatedMessage="Text de validation / d'explication de l'erreur"
/>
  )
}

function ButtonValidate () {
  return (
    <Button 
        style={{
            marginTop: '1em',
            textAlign: 'center'
        }} 
        onClick={function noRefCheck(){}}>Valider
    </Button>
  )
}
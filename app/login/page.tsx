"use client";

import * as React from "react";
import { Input } from "@codegouvfr/react-dsfr/Input";
import { PasswordInput } from "@codegouvfr/react-dsfr/blocks/PasswordInput"
import { Button } from "@codegouvfr/react-dsfr/Button";
import { FranceConnectButton } from "@codegouvfr/react-dsfr/FranceConnectButton";

export default function Page() {

    return(
        <div>
            <h1>Se connecter</h1>
            <InputLogin />
            <InputPassword />
            <ButtonLogin/>
            <ButtonRegister/>  
            <FranceConnect/>
        </div>
    )
        
}

function InputLogin () {
    return (
    <Input
        hintText="Par exemple : nom@example.com"
        label="Votre email personnel"
        state="default"
        stateRelatedMessage="Text de validation / d'explication de l'erreur"
      />
      )
}

function InputPassword () {
    return (
        <PasswordInput label="Votre mot de passe" />
      )
}

function ButtonLogin () {
    return (
        <Button 
            style={{
                marginTop: '1em',
            }} 
            onClick={function noRefCheck(){}}>Connexion
        </Button>
    )
}

function ButtonRegister () {
    return (
        <Button 
            style={{
                marginTop: '1em',
                display: 'block'
            }} 
            priority="secondary"
            linkProps={{
                href: '/register'
              }}
            >
              Créer un compte
            
        </Button>
    )
}

function FranceConnect () {
    return (
        
        <FranceConnectButton
        style={{
            marginTop: '3em',
            textAlign: 'center'
          }} 
        url="https://example.com" />
    )
}
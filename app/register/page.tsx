"use client";

import * as React from "react";
import { Input } from "@codegouvfr/react-dsfr/Input";
import { PasswordInput } from "@codegouvfr/react-dsfr/blocks/PasswordInput"
import { Button } from "@codegouvfr/react-dsfr/Button";
import { FranceConnectButton } from "@codegouvfr/react-dsfr/FranceConnectButton";

export default function Page() {

    return(
        <div>
            <h1>Créer un compte</h1>
            <InputName />
            <InputLogin />
            <InputPassword />
            <ButtonRegister/>  
            <FranceConnect/>
        </div>
    )
        
}

function InputName () {
    return (
    <Input
        hintText="Par exemple : Jean Dupont"
        label="Veuillez saisir votre prénom et nom"
        state="default"
        stateRelatedMessage="Text de validation / d'explication de l'erreur"
      />
      )
}

function InputLogin () {
    return (
    <Input
        hintText="Par exemple : nom@example.com"
        label="Veuillez saisir votre email personnel"
        state="default"
        stateRelatedMessage="Text de validation / d'explication de l'erreur"
      />
      )
}

function InputPassword () {
    return (
        <PasswordInput label="Veuillez saisir un mot de passe" />
      )
}

function ButtonRegister () {
    return (
        <Button 
            style={{
                marginTop: '1em',
                display: 'block'
            }} 
            linkProps={{
                href: '/register'
              }}
            >
              Créer un compte
            
        </Button>
    )
}

function FranceConnect () {
    return (
        
        <FranceConnectButton
        style={{
            marginTop: '3em',
            textAlign: 'center'
          }} 
        url="https://example.com" />
    )
}
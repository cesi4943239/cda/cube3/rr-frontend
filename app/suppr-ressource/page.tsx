import apiCall from "#/lib/apiCall";
import RessourceCard from "#/components/ressourceCard";
import { Ressource } from "#/types/ressource";

export default async function Page() {

  const ressources = await apiCall({method: "GET", url: "/resources?limit=03&page=2"});

  return (
    ressources && ressources.data.length > 0
        ? ( 
          <div>
            {ressources.data.map((ressource: Ressource) => 
            { 
              return <RessourceCard key={ressource.id} ressource={ressource} />})}              
          </div>
        )
        : ressources.data?.length == 0 
            ? ( <div>Aucune ressource est disponible.</div> )
            : ( <div>Chargement...</div> )
  )
}

"use client";

import { MainNavigation } from "@codegouvfr/react-dsfr/MainNavigation";
import { useSelectedLayoutSegment } from "next/navigation";

export function Navigation() {

	const segment = useSelectedLayoutSegment();

	return (
		<MainNavigation
			items={[
				{
					"text": "Page d'accueil",
					"linkProps": {
						"href": "/"
					},
					"isActive": segment === null
				},
				{
					"text": "Créer une ressource",
					"linkProps": {
						"href": "/creer-ressource"
					},
					"isActive": segment === "creer-ressource"
				},
				{
					"text": "Consulter une ressource",
					"linkProps": {
						"href": "/consulter-ressource"
					},
					"isActive": segment === "consulter-ressource"
				},
				{
					"text": "Supprimer une ressource",
					"linkProps": {
						"href": "/suppr-ressource"
					},
					"isActive": segment === "suppr-ressource"
				}
			]}
		/>
	);

}
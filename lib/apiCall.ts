//import { BASE_URL_API } from "@env";

type apiCall = {
    method: string,
    url: string,
    body?: object,
}

export default async function apiCall(request: apiCall): Promise<any> {
    const url = "http://localhost:3005" + request.url;
    console.log(url);
    return fetch(url, 
        {
            method: request.method,
            body: request.body ? JSON.stringify(request.body) : null,
            mode: "cors",
            headers: {
                "Content-Type": "application/json"
        },
    }).then((res) => {
        if (res.status == 400 || res.status == 401 || res.status == 404 || res.status == 409) return res.json();
        if (res.status == 200) {
            return res.json()
                    .catch((err) => { throw new Error(err) })
        } else {
            console.log(res.status)
            res.json().then((data) => console.log(data))
        }
    }).catch((err) => { throw err })
}